<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/profile', function () {
    return view('catizen.profile');
});



Route::get('/post/create', 'PostController@create'); // create form
Route::post('/post', 'PostController@store'); // add to database
Route::get('/post', 'PostController@index'); // show all item
Route::get('/post/{id}', 'PostController@show'); //{id}=>tidak harus {id} bebas, hanya sebatas parameter || nama tidak harus sama dengan yang dicontroller
Route::get('/post/{post_id}/edit', 'PostController@edit'); // edit
//Route::put('/posts/{post_id}', 'PostController@update'); // update
//Route::delete('/posts/{posts_id}','PostController@destroy'); // delete