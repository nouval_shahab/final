<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatizenStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catizen_stats', function (Blueprint $table) {
            $table->bigIncrements('cs_id');
            $table->unsignedBigInteger('catizen_id');
            $table->unsignedBigInteger('following')->default('0');
            $table->unsignedBigInteger('followers')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catizen_stats');
    }
}
