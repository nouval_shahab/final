<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatizenFollowingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catizen_following', function (Blueprint $table) {
            $table->bigIncrements('following_id');
            $table->unsignedBigInteger('catizen_id');
            $table->unsignedBigInteger('follow_catizen_id');
            $table->unsignedBigInteger('follow')->default('1');
            $table->unsignedBigInteger('block')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catizen_following');
    }
}
