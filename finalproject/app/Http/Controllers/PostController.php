<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;


class PostController extends Controller
{
     public function create()
    {
        return view('catizen.post');
    }

    //insert kedatabase pakai request
    public function store(Request $request)
    {
        //dd($request->all());
        // required = tidak bole kosong|| untuk validasi
              // untuk insert data
        /*$query = DB::table('posts')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "catizen_id" => 1
        ]);
        // redirec dengan pesan
        */

        $posts = new Post;
        $posts->judul = $request["judul"];
        $posts->isi = $request["isi"];
        $posts->catizen_id = 1;
        $posts->save();

        return redirect('/post')->with('success', 'Post berhasil disimpan');
    }

    public function index()
    {
        // ambil data|| paling kiri adalah var
        //$posts = DB::table('posts')->get();
        $posts = Post::all();
        //dd($posts); // select * from
        
        return view('catizen.index', compact('posts'));
    }

    //$id = param
    public function show($id)
    {
        //$posts = DB::table('posts')->where('post_id', $id)->first();  // select from .. where id = $id | first => tampilkan satu data object| kalao get menjadi colection array
        $posts = Post::where('post_id', $id)->first();
        //dd($posts);
        return view('catizen.show', compact('posts'));
    }

    public function edit($id)
    {
        $posts = DB::table('posts')->where('post_id', $id)->first();
        // dd($pertanyaans);
        return view('catizen.edit', compact('posts'));
    }

    
    public function update($id, Request $request)
    {

        /* $request->validate([
            'judul' => 'required|unique:pertanyaans',
            'isi' => 'required'
        ]);
		*/
        $posts = DB::table('posts')
            ->where('post_id', $id)
            ->update([
                'judul' => $request['judul'],
                'isi' => $request['isi']
            ]);
        return redirect('/index')->with('success', 'Berhasil Update Post');
    }

    public function destroy($id)
    {
        $pertanyaans = DB::table('posts')->where('post_id', $id)->delete();
        return redirect('/post')->with('success', 'Berhasil Hapus Post');
    }

}
